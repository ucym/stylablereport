/*jslint browser:true, vars:true, eqeq:true, nomen:true, expr:true */
/*global require */
require.config({
    paths : {
        "jQuery" : "lib/jquery-2.0.3.min",
        "marked" : "lib/marked",
        "underscore" : "lib/underscore-min"
    },
    shim : {
        "jQuery" : { exports: "jQuery"},
        "marked" : { exports: "marked"},
        "underscore" : { exports: "_"}
    }
});
(function () {
    var plugins = [
        "PageData",
        "DataBind",
        "Markdown",
        "InnerJson",
        "Template",
    ];
    
    try {
        require(["../user/user"]);
    } catch (e) {
        console.warn("Cant load user script (%s)", require.toUrl("../user/user"), e.toString());
    }

    require(plugins.map(function (name) { return "plugins/" + name;}));
}());