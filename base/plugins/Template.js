/*jslint browser:true, vars:true, eqeq:true, nomen:true, expr:true */
/*global define */
/**
 * Templateプラグイン
 * 
 * data-tpl-id属性が指定されたscript要素の内包テキストを
 * underscore.jsのテンプレートとして利用します。
 *
 * - テンプレートの定義
 *   <script type="text/html" data-tpl-id="任意のテンプレートID">
 *     <!-- underscore.jsの templateで解釈可能なHTML / JS -->
 *   </script>
 *
 * - テンプレートの埋め込み
 *   <div data-tpl-expand="展開するテンプレートのID"></div>
 *
 * - マジックテンプレートID
 *   IDが"_header"もしくは"_footer"のテンプレートが定義されると
 *   すべてのページのヘッダー、フッターに自動的にテンプレートが挿入・展開されます。
 *   これを防ぐ場合は、対象ページとなるarticle要素に"data-tpl-nomagic"属性を付与します。
 */
define(function (require, exports) {
    var $   = require("jQuery"),
        _   = require("underscore");
    
    var _templates  = {},
        _factories  = [];

    function _addDataFactory(fn) {
        if (typeof fn === "function" && _.indexOf(_factories, fn) === -1) {
            _factories.push(fn);
            _expanding();
        }
    }
    
    function _addTemplate(id, content) {
        if (_templates[id]) {
            console.error("Conflict template id: %s", id);
            return;
        }
        
        _templates[id] = _.template(content);
    }
    
    function _buildTemplate(id, data) {
        var tpl = _templates[id];
        
        if (typeof tpl === "undefined") {
            console.error("undefined template id:%s", id);
        }
        
        return typeof tpl === "function" && tpl(data);
    }
    
    function _expanding() {
        $("[data-tpl-expanded]").remove();
        
        $("[data-tpl-expand]").each(function () {
            var $self   = $(this),
                element = this,
                id      = $self.attr("data-tpl-expand"),
                data    = {};
            
            // 展開前処理を実行させる
            console.log(_factories);
            $.each(_factories, function () {
                this(
                    // 処理中の要素
                    element,
                    // 割り当てデータを追加する関数
                    function (newData) { _.extend(data, newData); },
                    // 割り当て中のデータのクローンを取得する関数
                    function () { return _.clone(data); }
                );
            });
            
            try {
                $(_buildTemplate(id, data))
                    .attr("data-tpl-expanded", id)
                    .insertAfter($self);
                console.info("Template %s expanding with ", id, data);
            } catch (e) {
                console.error("Template build failed: %s", e.toString(), _factories);
            }
        });
    }
    
    /**
     * 初期化処理
     */
    $(function () {
        console.log("Template initialize start.");
        
        // テンプレートの抽出
        $("script[data-tpl-id]").each(function () {
            var $self   = $(this),
                id      = $self.attr("data-tpl-id");
            
            _addTemplate(id, $self.html());
        });
        
        console.log("Template cached.");
        
        // マジックテンプレートの処理
        (function () {
            var $expander = $("<div />");
            
            if (_templates._header) {
                $expander.attr("data-tpl-expand", "_header");
                $("body > article:not([data-tpl-nomagic])").prepend($expander.clone());
            }
            
            if (_templates._footer) {
                $expander.attr("data-tpl-expand", "_footer");
                $("body > article:not([data-tpl-nomagic])").append($expander.clone());
            }
        }());
        
        // テンプレートの展開
        _expanding();
    });
    
    exports.addDataFactory = _addDataFactory;
    
    return exports;
});