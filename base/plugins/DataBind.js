/*jslint browser:true, vars:true, eqeq:true, nomen:true, expr:true */
/*global define */
/**
 * DataBind プラグイン
 *
 * テンプレートにInnerJsonプラグインで管理されているデータを割り当てます。
 * data-json-idで指定したIDをテンプレート定義（もしくは展開要素）のdata-json-bind属性に設定することで
 * テンプレートにJSONデータを割り当てます。
 *
 * テンプレート提議要素（data-tpl-id属性が設定されているscript要素）に割り当てたデータを基に
 * テンプレート展開要素（data-tpl-expand属性が設定されているscript要素）に割り当てたデータをマージ（上書き）していきます。
 *
 * - 設定例
 *   - JSONの定義
 *     <script type="text/json" data-json-id="default">
 *       {"title": "ホウセイ マイ フレンド", "summary" : "ちょっと待て。宇宙人が何か言ってる"}
 *     </script>
 *
 *     <script type="text/json" data-json-id="data-1">
 *       {"title" : "田中タイキック", "out" : true}
 *     </script>
 *
 *   - テンプレートの定義とデータの割り当て
 *     <script type="text/html" data-tpl-id="article" data-json-bind="default">
 *       <section>
 *         <h1 class="<%= out ? "red" : "" %>"><%= title %></h1>
 *         <p>
 *           <%= summary %>
 *         </p>
 *       </section>
 *     </script>
 *
 *   - テンプレートの挿入とデータの割り当て
 *     <div data-tpl-expand="article" data-json-bind="data-1"></div>
 *
 *   - 展開後
 *     <section>
 *       <h1 class="red">田中タイキック</h1>
 *       <p>
 *         宇宙人がなにか言っているぞ
 *       </p>
 *     </section>
 */
define(function (require) {
    var $           = require("jQuery"),
        Template    = require("./Template"),
        InnerJson   = require("./InnerJson");
    
    var _$tmpl = null;
    
    // テンプレートごとのデータバインド
    Template.addDataFactory(function (el, bindData) {
        _$tmpl = _$tmpl || $("[data-tpl-id]");
        
        var tplId = el.getAttribute("data-tpl-expand"),
            jsonId,
            data;
        
        // 元の要素に割り当てられたJsonIdを取得
        jsonId = _$tmpl.filter(function () {
            return this.getAttribute("data-tpl-id") === tplId;
        })
            .attr("data-json-bind");
        
        if (jsonId) {
            // データを取得
            data = InnerJson.get(jsonId);
            typeof data !== "undefined" && bindData(data);
        }
    });
    
    // テンプレート展開先ごとのデータバインド
    Template.addDataFactory(function (el, bindData) {
        _$tmpl = _$tmpl || $("[data-tpl-id]");
        
        var jsonId = el.getAttribute("data-json-bind"),
            data;
        
        if (jsonId) {
            data = InnerJson.get(jsonId);
            typeof data === "undefined" && bindData(data);
        }
    });
});