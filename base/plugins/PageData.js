/*jslint browser:true, vars:true, eqeq:true, nomen:true, expr:true */
/*global define */
define(function (require, exports) {
    var $   = require("jQuery"),
        _   = require("underscore"),
        Template = require("./Template");
    
    var _genFn = [];
    
    // ページ内のテンプレート展開要素を検索して
    // テンプレート展開イベント
   
    exports.regist = function (fn) {
        if(typeof fn === "function" && _.indexOf(_genFn, fn) === -1) {
            Template.addDataFactory(function (el, bindData, getData) {
                var page = $(el).parents().filter("body > article");
                
                try {
                    bindData(fn(page,  getData()) || {});
                } catch (e) {
                    console.error("Error on generator(%s)", e.toString());
                }
            });
        }
    };
    
    exports.basicGens = {
        // ページ番号取得
        pageNo : function () {
            exports.regist(function (page, data) {
                var _$pages = $("body > article"); 
                
                return {
                    pageNo : {all: _$pages.length, now: _$pages.index(page) + 1}
                };
            });
        }
    };

    return exports;
});