/*jslint browser:true, vars:true, eqeq:true, nomen:true, expr:true */
/*global define */
/**
 * InnerJsonプラグイン
 * 
 * data-json-id属性が指定されたscript要素の内包テキストを
 * jsonデータとして利用します。（データの定義を必ず参照してください）
 *
 * - インラインJSONの定義
 *   <script type="text/json" data-json-id="任意のID">
 *   -- JSONデータ
 *   </script>
 *
 *   ※必ず[type="text/json"]を指定してください。"application/json"は回りくどいです。
 * 
 * - データの取得(javascript、例)
 *   require(["plugins/InnerJson"], function (InnerJson) {
 *     var data = InnerJson.get("data-json-idで指定したID");
 *   })
 * 
 */
define(function (require, exports) {
    var $   = require("jQuery"),
        _   = require("underscore");
    
    var _dfd    = $.Deferred(),
        _jsons  = {};
     
    function _getJson(id) {
        if (typeof _jsons[id] === "undefined") {
            console.error("undefined json data id:%s", id);
            return;
        }
        
        return _.clone(_jsons[id]);
    }
    
    $(function () {
        $("script[type='text/json'][data-json-id]").each(function () {
            var $self   = $(this),
                id      = $self.attr("data-json-id");
            
            try {
                _jsons[id] = JSON.parse($self.text());
            } catch (e) {
                console.error("JSON parse error on json-id:%s (%s)", id, e.toString());
            }
        });
        
        _dfd.resolve(exports);
    });
    
    exports.get = _getJson;
    exports.promise = _dfd.promise.bind(_dfd);
    
    return exports;
});