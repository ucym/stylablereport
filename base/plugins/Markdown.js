/*jslint browser:true, vars:true, eqeq:true, nomen:true, expr:true */
/*global define */
/**
 * Markdownプラグイン
 *
 * type属性に"text/markdown"を設定したscript要素のMarkdownを
 * MarkdownからHTML（divに内包）に変換して、元のscript要素と入れ替えます。
 */
define(function (require) {
    var $       = require("jQuery"),
        Marked  = require("marked");

    $(function () {
        $("script[type='text/markdown']").each(function () {
            $("<div />")
                .addClass("markdown")
                .html(Marked(this.innerHTML))
                .insertBefore(this);

            this.remove();
        });
    });
});